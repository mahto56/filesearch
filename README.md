# FileSearcher
A crawler for crawling text contents from a directory recursively

## Run
* Clone the project

* This is a `maven` project, you can open it in any modern IDE as maven project and the IDE will do the rest for you
	* Example: In netbeans , click on File > Open Project > Select the project directory > OK
		* Run > Build Project > Run Project
		* Visit `http://localhost:8084/FileSearch/`


* To handle cors error add the following to your elasticsearch.yml
	* 
	```	
		http.cors.enabled : true
		http.cors.allow-origin: "*"
		http.cors.allow-methods: OPTIONS, HEAD, GET, POST, PUT, DELETE
		http.cors.allow-headers: X-Requested-With,X-Auth-Token,Content-Type,Content-Length
		http.cors.allow-credentials: true
	```
* Also create a books index in elasticsearch
	* ```
		curl -X PUT localhost:9200/books
	 ```

* To index new directory click on `+add` button. 
	* Enter the Directory Path > add.
	* You can now go back to main page, where you can continue your search.
	* The crawling is done in the background.