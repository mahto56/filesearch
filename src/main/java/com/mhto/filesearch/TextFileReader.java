/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mhto.filesearch;

/**
 *
 * @author Arvind Mahto
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TextFileReader  {

    private final File f;
    private final BufferedReader bf;
    private String nextLine;
    private BigInteger lineNo;
    private final int readSize;
    private final String filename;
    private final ArrayList<FileBean> fb_list;

    TextFileReader(String filename, int readSize) throws FileNotFoundException, IOException {
        f = new File(filename);
        this.filename = filename;
        bf = new BufferedReader(new FileReader(f));
        fb_list = new ArrayList();
        nextLine = bf.readLine();
        lineNo = BigInteger.ONE;
        this.readSize = readSize;
    }

    public boolean hasNext() {
        return this.nextLine != null;
    }

    public List<FileBean> getNext() throws Exception {
        this.fb_list.clear();
        int lineCount = 0;
        while (lineCount < this.readSize && this.nextLine != null) {
            String id = f.getName()+"_"+lineNo;
            FileBean fb = new FileBean(id,lineNo, nextLine, filename);
            ++lineCount;
            this.lineNo = this.lineNo.add(BigInteger.ONE);
            this.fb_list.add(fb);
            this.nextLine = this.bf.readLine();
        }
        return this.fb_list;
    }
    
    public void close(){
        try {
            bf.close();
        } catch (IOException ex) {
            Logger.getLogger(TextFileReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
//test code    
//    public static void main(String args[]) throws IOException, Exception {
//        String filename = "E:\\LargeFileTest\\l.txt";
//        AltLogFileReader altReader = new AltLogFileReader(filename, 1000);
//        while (altReader.hasNext()) {
//            for (LogBean lb : altReader.getNext()) {
////                System.out.println(lb);
//            }
//
//        }
//        System.out.println("done");
//    }

}