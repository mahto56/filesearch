/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mhto.filesearch;

import com.google.gson.Gson;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.common.xcontent.XContentType;

/**
 *
 * @author Arvind Mahto
 */
public class RecursiveDirectoryIndexTask extends RecursiveAction {

    private final String root;
    private final int readSize = 1000;
    private final BulkProcessor bulkProcessor;
    private final String indexName;
    private final Gson gson;

    public RecursiveDirectoryIndexTask(String root, BulkProcessor bulkProcessor, String indexName, Gson gson) {
        this.root = root;
        this.bulkProcessor = bulkProcessor;
        this.indexName = indexName;
        this.gson = gson;
    }

    @Override
    protected void compute() {
        List<RecursiveDirectoryIndexTask> actionlist = new ArrayList<>(); // a list for keeping track of child threads
        DirectoryStream<Path> stream; //To iterate over contents of directory

        try {
            stream = Files.newDirectoryStream(Paths.get(root));
            for (Path path : stream) {
                if (!Files.isDirectory(path) && path.toString().endsWith("txt")) {
                    //index each file here
                    TextFileReader tfr = new TextFileReader(path.toString(), readSize);
                    while (tfr.hasNext()) {
                        for (FileBean fb : tfr.getNext()) {
                            String jsonData = gson.toJson(fb);
                            IndexRequest indexRequest = new IndexRequest(indexName)
                                    .id(fb.getID())
                                    .source(jsonData, XContentType.JSON);
                            bulkProcessor.add(indexRequest);

                        }
                    }
                    tfr.close();
                } else if (Files.isDirectory(path)) {
                    RecursiveDirectoryIndexTask childAction = new RecursiveDirectoryIndexTask(path.toString(), bulkProcessor, indexName, gson);
                    actionlist.add(childAction);
                    childAction.fork(); //start child thread in root thread
                }
            }
            stream.close();
        } catch (IOException ex) {
            Logger.getLogger(RecursiveDirectoryIndexTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(RecursiveDirectoryIndexTask.class.getName()).log(Level.SEVERE, null, ex);
        }

        actionlist.forEach((t) -> {
            t.join(); // wait for all its child to complete
        });
    }

}
