/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mhto.filesearch;

import java.math.BigInteger;

/**
 *
 * @author Arvind Mahto
 */
class FileBean {
    private final BigInteger lineNo;
    private final String text;
    private final String fileName;
    private final String id;

    public FileBean(String id,BigInteger lineNo, String text, String fileName) {
        this.id = id;
        this.lineNo = lineNo;
        this.text = text;
        this.fileName = fileName;
    }

    String getID() {
        return this.id;
    }
}
