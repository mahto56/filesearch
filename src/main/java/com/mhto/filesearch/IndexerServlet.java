/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mhto.filesearch;

import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ForkJoinPool;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BackoffPolicy;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;

/**
 *
 * @author Arvind Mahto
 */
public class IndexerServlet extends HttpServlet {

    private String indexName;
    private ForkJoinPool commonPool;
    private BulkProcessor.Listener bulkProcessorListener;
    private int readSize;
    private BulkProcessor bulkProcessor;
    private RestHighLevelClient client;
    private Gson gson;
    private BlockingQueue<Runnable> queue; //A queue for running background tasks
    private SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        RequestDispatcher view = request.getRequestDispatcher("add.html");
        view.forward(request, response);
    }

    

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.setContentType("application/json;charset=UTF-8");
        
        try (PrintWriter pw = res.getWriter()) {
            //req.getContextPath();
            String dirpath = req.getParameter("filepath");
            
            //if queue is full, dont take new tasks
            if(queue.size()==5){
                pw.write("{\"alert\":true,\"message\":\"Queue is full.. Please try after some time ...\"}");
            }
            else if (dirpath != null) {
                File file = new File(dirpath);
                if (file.exists() && file.isDirectory()) {
                    RecursiveDirectoryIndexTask fa = new RecursiveDirectoryIndexTask(dirpath, bulkProcessor,indexName, gson);
                    //run new task in sequence
                    Logger.getLogger(IndexerServlet.class.getName()).log(Level.INFO, "{0} Task in Queue, Adding new Task to the Queue", queue.size());
                    queue.add(()->{
                        Logger.getLogger(IndexerServlet.class.getName()).log(Level.INFO, "Task Added for directory {0} at {1}", new Object[]{file.getName(), formatter.format(new Date())});
                        commonPool.invoke(fa);
                        Logger.getLogger(IndexerServlet.class.getName()).log(Level.INFO, "Task Finised for directory {0} at {1}", new Object[]{file.getName(), formatter.format(new Date())});
                    });
                        
                    pw.write("{\"alert\":false,\"message\":\"Directory contents are being added in the background ...\"}");
                
                }else{
                    pw.write(String.format("{\"alert\":true,\"message\":\"%s is not a valid directory!\"}",dirpath));
                }


            } else {
                pw.write("{\"alert\":true,\"message\":\"filepath can't be empty!\"}");
            }

        }
    }

    @Override
    public void init() throws ServletException {
//        super.init();
        indexName = "books";
        readSize = 1000;
        gson = new Gson();
        commonPool = ForkJoinPool.commonPool();
        bulkProcessorListener = new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long executionID, BulkRequest br) {
                int totalActions = br.numberOfActions();
//                System.out.println(String.format("Executing bulk [%s] with %s requests", executionID, totalActions));
            }

            @Override
            public void afterBulk(long executionID, BulkRequest br, BulkResponse bresp) {
                if (bresp.hasFailures()) {
                    System.out.println(String.format("Bulk [%s] executed with failures", executionID));
                } else {
//                    System.out.println(String.format("Bulk [%s] completed in %s milliseconds", executionID, bresp.getTook().getMillis()));
                }

            }

            @Override
            public void afterBulk(long executionID, BulkRequest br, Throwable failure) {
                System.out.println(String.format("Failed to execute Bulk [%s] , %s", executionID, failure));
                
            }
        };
        
       client = new RestHighLevelClient(
                        RestClient.builder(new HttpHost("localhost", 9200, "http")));
        
       bulkProcessor = BulkProcessor
                .builder(
                        (request, listener) -> {
                            client.bulkAsync(request, RequestOptions.DEFAULT, listener);
                        },
                        this.bulkProcessorListener
                )
                .setBulkActions(readSize)
                .setFlushInterval(TimeValue.timeValueSeconds(10L))
                .setBackoffPolicy(BackoffPolicy.constantBackoff(TimeValue.timeValueSeconds(1L), 3))
                .setConcurrentRequests(10)
                .build();
      
       //queue runs a task if available in Queue
       queue = new ArrayBlockingQueue<>(5);
       new Thread(() -> {
              while(true){
                  try {
                      queue.take().run();
                      
                  } catch (InterruptedException ex) {
                      Logger.getLogger(IndexerServlet.class.getName()).log(Level.SEVERE, null, ex);
                  }
                  
              }
        }).start();
    }

    @Override
    public void destroy() {
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
        bulkProcessor.close();
                        
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
