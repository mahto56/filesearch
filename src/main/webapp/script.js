/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function handleSearchSubmit() {
    $('#searchform').on("submit", e => {
        e.preventDefault();
        let jsonData = {
            query: {
                match: {
                    text: {
                        query: $("#searchbox").val(),
                        analyzer: "standard",
                        operator: "and",
                        
                    }
                }
            },
            highlight: {
                fields: {
                    text: {}
                },
//                fragment_size: 50,
                pre_tags: ['<b>'],
                post_tags: ['</b>']
            },
            size: $("#size option:selected").val()

        };
        console.log($('size').children("option:selected").val());
        $('#content').html('loading..');

        $.ajax({
            url: 'http://127.0.0.1:9200/books/_search/',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                if (data.hits.total.value === 0)
                    $('#content').html("No result found!");
                else {
                    $('#content').html("");
                    data.hits.hits.map(h => {
                        console.log(h);
                        $('#content').append(
                                `<div class='content-data'>
                                    *  <span class='italics large'>
                                        "${h.highlight.text[0]}"
                                    </span> at line:${h._source.lineNo} in file: <span class="highlight">${h._source.fileName}<span></div>`);
                    });
                }

                console.log("recieved: ", data);
            },
            error: function (jqXHR, status, error) {
                if (jqXHR.status === 500) {
                    alert('Internal error: ' + jqXHR.responseText);
                } else {
                    alert(error);
                }
            },
            data: JSON.stringify(jsonData)
        });
        console.log(jsonData);
    });
}


function handleAddSubmit() {
    $('#addform').on("submit", e => {
        e.preventDefault();
        let form = $(this);
        let url = form.attr('action');

//        $('#content').html('loading..');
        $.ajax({
            url: url,
            type: 'post',
            data: $('#addform').serialize(),
            success: function (data) {
                if (data.alert) {
                    alert(data.message);
                } else
                    $('#add-content').html(`<div class="text-padded">${data.message}</div>`);
                console.log("recieved: ", data);
            },
            error: function (jqXHR, status, error) {
                if (jqXHR.status === 500) {
                    alert('Internal error: ' + jqXHR.responseText);
                } else {
                    alert(error);
                }
            }

        });

    });
}




handleSearchSubmit();
handleAddSubmit();